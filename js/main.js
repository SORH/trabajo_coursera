var id;

function conseguirID(comp){
  id = comp.id;
  console.log(id)
};

$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $('.carousel').carousel({interval:5000});
  $('#inscripcion').on('show.bs.modal',function(e){
    console.log('El modal se esta mostrando');
    $('#'+id).removeClass('btn-outline-success');
    $('#'+id).addClass('btn-primary');
    $('#'+id).prop('disabled',true);
  });

  $('#inscripcion').on('shown.bs.modal',function(e){
    console.log('El modal se mostró');
  });

  $('#inscripcion').on('hide.bs.modal',function(e){
    console.log('El modal se esta ocultando');
  });

  $('#inscripcion').on('hidden.bs.modal',function(e){
    console.log('El modal se ocultó');
    $('#'+id).removeClass('btn-primary');
    $('#'+id).addClass('btn-outline-success');
    $('#'+id).prop('disabled',false);
  });

});
